var user = null;
//{
//		nik : '',
//        name : '',
//        accountNo : '',
//        username : '',
//        password : '',
//        phone : '',
//        email : '',
//        address : ''	
//};

var kampusGlobal = {
	name: 'UTS MP 1118100126',
	author: 'Choirul Hafifi',
	client: 'Mahasiswa',
	error: '',
	contact: {
		email: 'choirulhafifi29@gmail.com',
		phone: '089621806536',
		address: 'Jl.Mas Mangun Negara',
		ig: 'chocreat.ive',
		tw: '@choirulhafifi',
		li: '',
		fb: 'choirulhafifi',
		yt: 'choirulhafifi',
		website: ''
	},
	footer: {
		year: '2020',
		name: 'Mobile Programming',
		website: '',
	},
	about: {
		description: '',
		vision: '',
		mission: ''
	},
	timeout: {
		connect: null,
		read: null
	},
	session: {
		loggedUser: null,
		deviceId: null,
		loginTime: null
	},
	alert: {
		system: {
			notFound: 'Page Not Found',
			internalError: 'Internal Error',
			underConstruction: 'Ooops... The page is under construction',
			invalidInput: 'You input invalid data',
			underMaintenance: 'System under maintenance, Come again later',
			differentVersion: 'Your device using deprecated version (Minimum Android 6 (Marshmellow))'
		},
		login: {
			success: 'Login Success',
			failed: 'Login Failed',
			invalidUsername: 'Invalid Username',
			invalidPassword: 'Invalid Password',
			emptyUsername: 'Isi Dulu Hayooo NIM nya',
			emptyPassword: 'Isi Dulu Hayooo Password nya',
		},
		common: {
			success: 'Success',
			fail: 'Failed',
			pending: 'On Progress',
			timeout: 'Timeout'
		}
	}
}

var dataInhouse = null;