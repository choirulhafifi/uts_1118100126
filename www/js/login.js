$$(document).on('page:init', '.page[data-name="login"]', function () {
	$$("#login-btn").on('click', function () {
		var form = app.form.convertToData('#login-form');
		if (form.username == null || form.username.trim() == '') {
			app.dialog.alert(kampusGlobal.alert.login.emptyUsername, kampusGlobal.error, function () { });
			return;
		}

		if (form.password == null || form.password.trim() == '') {
			app.dialog.alert(kampusGlobal.alert.login.emptyPassword, kampusGlobal.error, function () { });
			return;
		}

		app.views.main.router.navigate('/dashboard/', { reloadCurrent: true, transition: 'f7-push' });

	});

});
