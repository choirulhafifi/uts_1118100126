var routes = [
  // Index page
  {
    path: '/',
    url: './index.html',
    name: 'home',
  },
  {
    path: '/login/',
    url: './pages/login.html',
    name: 'login',
    options: {
      transition: 'f7-push',
    },
  },
  {
    path: '/register/',
    url: './pages/register.html',
    name: 'register',
  },
  {
    path: '/dashboard/',
    url: './pages/dashboard.html',
    name: 'dashboard',
  },
  {
    path: '/account/',
    url: './pages/account.html',
    name: 'account',
  },
  {
    path: '/pay/',
    url: './pages/pay.html',
    name: 'pay',
  },
  {
    path: '/history/',
    url: './pages/history.html',
    name: 'history',
  },
  {
    path: '/history/:id/',
    componentUrl: './pages/history_list.html',
  },
  {
    path: '/prodi/',
    url: './pages/prodi.html',
    name: 'prodi',
  },
  {
    path: '/jadwal/',
    url: './pages/jadwal.html',
    name: 'jadwal',
  },
  // Default route (404 page). MUST BE THE LAST
  {
    path: '(.*)',
    url: './pages/404.html',
  },
];
