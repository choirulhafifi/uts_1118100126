$$(document).on('page:init', '.page[data-name="dashboard"]', function () {

	$$("#btn-pay").on('click', function () {
		user = user;
		app.views.main.router.navigate('/pay/', { reloadCurrent: true, transition: 'f7-circle' });
	});

	$$("#history-btn").on('click', function () {
		user = user;
		app.views.main.router.navigate('/history/', { reloadCurrent: true, transition: 'f7-circle' });
	});

	$$("#prodi-btn").on('click', function () {
		user = null;
		app.views.main.router.navigate('/prodi/', { reloadCurrent: true, transition: 'f7-circle' });
	});
	$$("#jadwal-btn").on('click', function () {
		user = null;
		app.views.main.router.navigate('/jadwal/', { reloadCurrent: true, transition: 'f7-circle' });
	});

	$$("#logout-btn").on('click', function () {
		user = null;
		app.views.main.router.navigate('/login/', { reloadCurrent: true, transition: 'f7-push' });
	});

});
